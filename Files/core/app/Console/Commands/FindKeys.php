<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Finder\Finder;
use Illuminate\Support\Facades\Storage;

class FindKeys extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:find {locale=en}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->findTranslations();

        return 0;
    }



    public function findTranslations($path = null)
    {
        $path = $path ? : base_path();
        $keys = array();
        $functions = array(
            'trans',
            'trans_choice',
            'Lang::get',
            'Lang::choice',
            'Lang::trans',
            'Lang::transChoice',
            '@lang',
            '@choice',
            'transEditable',
            '__'
        );
        $pattern =                              // See http://regexr.com/392hu
            "[^\w]" .                          // Must not have an alphanum or _ or > before real method
            "(" . implode('|', $functions) . ")" .  // Must start with one of the functions
            "\(" .                               // Match opening parenthese
            "[\'\"]" .                           // Match " or '
            "(" .                                // Start a new group to match:
                ".+".               // Must start with group
//            "([^\1)]+)+" .                // Be followed by one or more items/keys
            ")" .                                // Close group
            "[\'\"]" .                           // Closing quote
            "[\),]";                            // Close parentheses or new parameter
        // Find all PHP + Twig files in the app folder, except for storage
        $finder = new Finder();

        $finder->in($path)->exclude('storage')
            ->exclude('node_modules')
            ->exclude('public')
            ->exclude('test')
            ->name('*.php')->files();
        /** @var \Symfony\Component\Finder\SplFileInfo $file */
        foreach ($finder as $file) {
            // Search the current file for the pattern
            if (preg_match_all("/$pattern/siU", $file->getContents(), $matches)) {
                // Get all matches
                foreach ($matches[2] as $key) {
                    if(!$key) continue;
                    $keys[] = $key;
                }
            }
        }
        // Remove duplicates
        $keys = array_values(array_unique($keys));
        // Add the translations to the database, if not existing.

        file_put_contents(resource_path("lang/{$this->argument('locale')}.json"), json_encode(array_combine($keys,$keys)));
        // Storage::disk(resource_path("lang"))->put("{$this->argument('locale')}.json", json_encode($keys));
        // Return the number of found translations
        $count =  count($keys);

        $this->info("$count keys founded");
    }

}
