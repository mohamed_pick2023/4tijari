@extends($activeTemplate.'layouts.frontend')
@section('content')
@php
	$banner = getContent('banner.content',true)->data_values;
	$divisions = \App\Models\Division::where('status',1)->get();
    $District = \App\Models\District::where('status',1)->get();
@endphp
  <section class="hero bg_img" style="background-image: url({{getImage('assets/images/frontend/banner/'.$banner->background_image,'1920x1080')}});">
	<div class="container-fluid">
	  <div class="row justify-content-center">

		<div class="col-xxl-6 col-lg-8 text-center">
		  <span class="hero__subtitle">{{__($banner->title)}}</span>
		  <h2 class="hero__title">{{__($banner->heading)}}</h2>
		  <p class="hero__description">{{__($banner->sub_heading)}}</p>
		</div>

	  </div><!-- row end -->
	  <div class="row justify-content-center mt-5">

		<div class="col-xl-8 col-lg-10">

		  <form class="hero-search-form" action="" method="GET" id="searchForm">
			<div class="input-group ps-sm-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                    <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                </svg>
			  <input type="text" name="search" placeholder="@lang('Enter keyword, title')" autocomplete="off" class="form--control border-none" >
			</div>
			<div class="input-group ps-sm-3">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-filter" viewBox="0 0 16 16">
                    <path d="M6 10.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/>
                  </svg>
              {{-- Type --}}
                <select class="select" name="division">
                    <option selected>{{ __('Category') }}</option>
                    @foreach ($divisions as $div)
                    <option value="{{$div->slug}}">{{$div->name}}</option>
                    @endforeach
                </select>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                    <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                </svg>
                {{-- location --}}

                <select class="select" name="location">
                    <option value="location">{{ __('Location') }}</option>
                    @foreach ($District as $div)
                    <option value="{{$div->slug}}">{{$div->name}}</option>
                    @endforeach
                </select>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                    <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                </svg>
                {{-- price --}}

                <button class="price-toggle toggle-btn select " >{{ __('Price') }}

                </button>
                <svg  class="toggle-btn" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                    <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                </svg>
                <div class="range-slider">
                    <div class="min-value-label">min</div>
                    <div class="max-value-label">max</div>
                    <div id="min-value"></div>
                    <div id="max-value"></div>
                    <br>
                    <input type="range" class="max-price price-input" name="min" value="0" min="0" max="100000" step="1">
                    <input type="range" class="min-price price-input" name="max" value="100000" min="0" max="100000" step="1">
                  </div>
			</div>
			<button type="submit" class="hero-search-form-btn"> @lang('Search')</button>
		  </form>
		  <p class="text-white font-size--16px mt-3 text-center">{{__($banner->popular_keyword)}}</p>

		</div>

	  </div>
	</div>
  </section>
	@php
		$searchUrl =  http_build_query(request()->except('search'));
		$searchUrl =   str_replace("amp%3B","",$searchUrl);
		$queryStrings = json_encode(request()->query());
	@endphp
	@push('script')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/iziToast.min.css') }}">
    <script src="{{ asset('assets/admin/js/iziToast.min.js') }}"></script>
        @if (session()->has("response") && session()->get("response")['IsSuccess'] =='true')
            <script>
               var message = '{{ session()->get("response")["Message"] }}';
                iziToast.success({
                    message: message,
                    position: "topRight"
                });
            </script>
        @endif

        @if (session()->has("response") && session()->get("response")['IsSuccess'] =='false')
            <script>
                var message = '{{ session()->get("response")["Message"] }}';
                iziToast.error({
                    message: message,
                    position: "topRight"
                });
            </script>
        @endif

        <script>
            let minValue = document.getElementById("min-value");
            let maxValue = document.getElementById("max-value");

            function validateRange(minPrice, maxPrice) {
            if (minPrice > maxPrice) {
                // Swap to Values
                let tempValue = maxPrice;
                maxPrice = minPrice;
                minPrice = tempValue;
            }

            minValue.innerHTML = "$" + minPrice;
            maxValue.innerHTML = "$" + maxPrice;
            }

            const inputElements = document.querySelectorAll(".price-input");

            inputElements.forEach((element) => {
            element.addEventListener("change", (e) => {
                let minPrice = parseInt(inputElements[0].value);
                let maxPrice = parseInt(inputElements[1].value);

                validateRange(minPrice, maxPrice);
            });
            });

            validateRange(inputElements[0].value, inputElements[1].value);
        </script>
	<script>
		'use strict';
        $('.toggle-btn').on('click',function(e){
            e.preventDefault();
            $('.range-slider').fadeToggle(500);
        });
		$('#searchForm').on('submit',function(e){
			e.preventDefault();
			var data = $(this).serialize();
			var url = '{{url()->current()}}'+'/items/all'+'?{{$searchUrl}}';
			url = url.replaceAll('amp;','');
			var queryString = "{{$queryStrings}}"
			var delim;
			if(queryString.length > 2){
			delim = "&"
			}else {
			delim = ""
			}
			window.location.href = url+delim+data;
		});


	</script>
	@endpush

    @if($sections->secs != null)
        @foreach(json_decode($sections->secs) as $sec)
            @include($activeTemplate.'sections.'.$sec)
        @endforeach
    @endif
@endsection
