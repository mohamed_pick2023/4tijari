<?php

namespace App\Http\Controllers;

use MyFatoorah\Library\PaymentMyfatoorahApiV2;
use App\Models\GeneralSetting;
use App\Models\GatewayCurrency;
/**
 * @author Mohamed Hesham <ppick177@gmail.com>
 */
class MyFatoorahController extends Controller {

    public $mfObj,$price,$gate;
    public $payment_method = [
        0=>'ALL',
        1=>'KNET',
        2=>'VISA/MASTER',
        3=>'AMEX',
        4=>'BENEFIT',
        7=>'QATAR DEBIT CARDS',
        8=>'APPLE PAY',
        13=>'MEEZA'
    ];

//-----------------------------------------------------------------------------------------------------------------------------------------

    /**
     * create MyFatoorah object
     */
    public function __construct() {

        $this->mfObj = new PaymentMyfatoorahApiV2(
            config('myfatoorah.api_key'),
            config('myfatoorah.country_iso'),
            config('myfatoorah.test_mode')
        );

    }

//-----------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Create MyFatoorah invoice
     *
     * @return \Illuminate\Http\Response
     */
    public function index($price=null , $gateId) {
        try {

            $this->price  = $price;
            $fetch_gate =GatewayCurrency::where('id',$gateId)->select('name')->first();
            $payment_method = array_keys($this->payment_method,strtoupper($fetch_gate->name))[0];
            $paymentMethodId =  $payment_method;
            $curlData = $this->getPayLoadData();
            $data     = $this->mfObj->getInvoiceURL($curlData, $paymentMethodId);
            $response = ['IsSuccess' => 'true', 'Message' => 'Invoice created successfully.', 'Data' => $data];
            $url= $response['Data']['invoiceURL'];
            return redirect()->to($url);

        } catch (\Exception $e) {
            $response = ['IsSuccess' => 'false', 'Message' => $e->getMessage()];
        }
        return redirect()->route('home')->with('response',$response);

    }

//-----------------------------------------------------------------------------------------------------------------------------------------

    /**
     *
     * @param int|string $orderId
     * @return array
     */
    private function getPayLoadData($orderId = null) {

        $settings       = GeneralSetting::first();
        $callbackURL    = route('myfatoorah.callback');
        $user           = auth()->user();
        $currency       = strtoupper($settings->cur_text);
        $countryCode    = config('myfatoorah.country_code');
        $lang           = config('myfatoorah.lang');

        return [
            'CustomerName'       => $user->username,
            'InvoiceValue'       =>  $this->price,
            'DisplayCurrencyIso' => $currency,
            'CustomerEmail'      => $user->email,
            'CallBackUrl'        => $callbackURL,
            'ErrorUrl'           => $callbackURL,
            'MobileCountryCode'  => $countryCode,
            'CustomerMobile'     => substr($user->mobile,0,11),
            'Language'           => $lang,
            'CustomerReference'  => $orderId,
            'SourceInfo'         => 'Laravel ' . app()::VERSION . ' - MyFatoorah Package ' . MYFATOORAH_LARAVEL_PACKAGE_VERSION
        ];

    }

//-----------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Get MyFatoorah payment information
     *
     * @return \Illuminate\Http\Response
     */
    public function callback() {

        try {
            $paymentId = request('paymentId');
            $data      = $this->mfObj->getPaymentStatus($paymentId, 'PaymentId');

            if ($data->InvoiceStatus == 'Paid') {
                $msg = 'Invoice is paid.';
            } else if ($data->InvoiceStatus == 'Failed') {
                $msg = 'Invoice is not paid due to ' . $data->InvoiceError;
            } else if ($data->InvoiceStatus == 'Expired') {
                $msg = 'Invoice is expired.';
            }

            $response = ['IsSuccess' => 'true', 'Message' => $msg, 'Data' => $data];
        } catch (\Exception $e) {
            $response = ['IsSuccess' => 'false', 'Message' => $e->getMessage()];
        }
        return redirect()->route('home')->with('response',$response);

    }

//-----------------------------------------------------------------------------------------------------------------------------------------
}
